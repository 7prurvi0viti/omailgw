import { Login, IsLoggedIn } from "./api_connector.js";
import settings from './settings.js';

// Check if already loggedIn
if (IsLoggedIn()) {
  window.location.replace("index.html");
}

// use login div as button
document.getElementById("loginButton").addEventListener("click", async (event) => {
  event.preventDefault();

  // Show loading spinner
  const loadingSpinner = document.getElementById("loading-spinner");
  loadingSpinner.style.display = "block";
  
  // References html elements
  const email = document.getElementById("inputEmail").value;
  const password = document.getElementById("inputPassword").value;
  const errorMessage = document.getElementById("errorMessage");
  const rememberMe = document.getElementById("inputRememberPassword").checked;

  // Clear previous & show messages
  errorMessage.textContent = "";
  if (email === "") {
    errorMessage.textContent += "Please enter an email. ";
  }else if (!settings.emailRegex.test(email)){
    errorMessage.textContent += "Please enter a valid email adress. ";
  }
  if (password === "") {
    errorMessage.textContent += "Please enter a password. ";
  }
  if (errorMessage.textContent == "" && password.length < settings.password_min_length) {
    errorMessage.textContent = "Invalid credentials. Please try again.";
  }
  if (errorMessage.textContent != "") {
    return;
  };

  // Handle the API response
  // wait for the login function from api_connector.js
  let loginTry = await Login(email, password, rememberMe);
  if (loginTry[0]) { // first element in array = true => success
    window.location.replace("index.html");
    localStorage.setItem("userName", email);
  } else {
    const errorReading = settings.errorcodes[loginTry[1]];
    errorMessage.innerHTML = errorReading ? (errorReading + " ( Message from server : " + loginTry[2] + " )") : "Server connection failed.";
  }


  // Hide loading spinner
  loadingSpinner.style.display = "none";
});
