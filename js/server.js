import { GetServerList, GetServerAllSpool } from "./api_connector.js";
import settings from './settings.js';
import showRightInfos from './commun.js';

showRightInfos();

async function updateServer() {
  let getServer = await GetServerList();
  if (getServer[0]) {
    let dataGetServers = getServer[1];
    for (let dataGetServer of dataGetServers){
      $('#servers').append(
        "<h2 class='mt-4'>"+dataGetServer['hostname']+"</h2>" +
        "<p>Last connect : "+moment.unix(dataGetServer['last_cli_update']).format("YYYY-MM-DD hh:mm:ss")+"</p>" +
        "<div class='card mb-3'>" +
          `<div class='card-header' data-i18n-key="spooler">` +
            "<i class='fas fa-server'></i>Spooler :" +
          "</div>" +
          "<div class='card-body' id='"+dataGetServer['hostname']+"Spool'>" +
          "</div>" +
        "</div>"
      );
    }
    debug(dataGetServers);
    let getSpooler = await GetServerAllSpool();
    if (getSpooler[0]) {
      let dataGetSpoolers = getSpooler[1];
      for (let dataGetSpooler of dataGetSpoolers){
        $('#'+dataGetSpooler['hostname']+"Spool").append(

          '<div class="row">'+
            '<div class=col>'+
              'From : ' + dataGetSpooler['from']+
            '</div>'+
            '<div class="col text-center">'+
              'To : ' + dataGetSpooler['to']+
            '</div>'+
            '<div class="col text-end">'+
              '<label for="date"><span name="date">'+moment.unix(dataGetSpooler['date']).format("YYYY-MM-DD hh:mm:ss")+'</span>'+
            '</div>'+
          '</div>'+
          '<div class="row">'+
            '<div class="col">'+
              dataGetSpooler['msg']+
            '</div>'+
            '<div class="col text-end">'+
              '<a href="#one?queueId='+dataGetSpooler['queueId']+'">'+dataGetSpooler['queueId']+'</a>'+
            '</div>'+
          '</div>'+
          '<hr class="hr" />'
        );
      }
    } else {
      alert("Error on spooler loading data...")
    }
    debug(getSpooler);
  } else {
    alert("Error on server loading data...")
  }
}

jQuery(function() {
    $(document).ajaxStop(function () {
      $('#loadData').hide();
    });
    updateServer()
});
