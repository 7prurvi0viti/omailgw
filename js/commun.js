
// Check login and show user email

import { IsLoggedIn, Logout } from './api_connector.js';
import settings from './settings.js';

if (!IsLoggedIn()) {
    window.location.replace("login.html");
}

const logoutbtn = document.getElementById("logoutButton")
if (logoutbtn) {
    logoutbtn.addEventListener("click", async () => {
        let logoutTry = await Logout();
        //TODO modif l'affichage erreur ?
        if (logoutTry[0]) {
            window.location.replace("login.html");
        } else {
            const errorReading = settings.errorcodes[logoutTry[1]];
            alert(errorReading ? (errorReading + " ( Message from server : " + logoutTry[2] + " )") : "Server connection failed.");
        }
    });
};

window.addEventListener('DOMContentLoaded', event => {
    // Toggle the side navigation
    const sidebarToggle = document.body.querySelector('#sidebarToggle');
    if (sidebarToggle) {
        // Uncomment Below to persist sidebar toggle between refreshes
        if (localStorage.getItem('sb|sidebar-toggle') === 'true') {
            document.body.classList.toggle('sb-sidenav-toggled');
        }
        sidebarToggle.addEventListener('click', event => {
            event.preventDefault();
            document.body.classList.toggle('sb-sidenav-toggled');
            localStorage.setItem('sb|sidebar-toggle', document.body.classList.contains('sb-sidenav-toggled'));
        });
    }
    showRightInfos();
});

export default async function showRightInfos() {
    // If we are logged in, show username & role in every place required
    const usernameSpans = document.querySelectorAll(".username");
    const userName = localStorage.getItem("userName");
    const userRoleSpans = document.querySelectorAll(".userRole");
    const userRole = localStorage.getItem("userRole");
    for (var i = 0; i < usernameSpans.length; i++) {
        usernameSpans[i].textContent = userName;
    }
    for (var i = 0; i < userRoleSpans.length; i++) {
        switch (userRole) {
            case 'root':
                userRoleSpans[i].classList.add('text-warning');//orange
                break;
            case 'admin':
                userRoleSpans[i].classList.add('text-primary');//blue
                break;
            case 'user':
                userRoleSpans[i].classList.add('text-success');//green
                break;
            case 'mailgw':
                userRoleSpans[i].classList.add('text-muted');//grey
                break;
            default:
                break;
        }
        userRoleSpans[i].textContent = "( " + userRole + " )";
    }
    // show div depending on role visibility
    const userCanSee = settings.ui_visibility[userRole].sees.split(',');
    const visibleByRoles = document.querySelectorAll(".visibleByRole");
    debug("Visibility setup : As " + userRole + ", showing [" + userCanSee + "] => changing visibility of " + visibleByRoles.length + " elements");
    //find the elements
    var modified = "Applied visibility : "
    for (var i = 0; i < visibleByRoles.length; i++) {
        const visibility = visibleByRoles[i].getAttribute('data-visibility');
        //if not in userCanSee, hide it
        if (!userCanSee.includes(visibility)) {
            visibleByRoles[i].style.display = 'none';
            modified += (i + " hidden as " + visibility + ", ");
        } else {
            modified += (i + " visible as " + visibility + ", ");
        }
    };
    debug(modified);
}
